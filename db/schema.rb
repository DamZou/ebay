# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160530205748) do

  create_table "bids", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.decimal  "bid",               precision: 10, scale: 2, null: false
    t.integer  "user_id",                                    null: false
    t.integer  "product_id",                                 null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.decimal  "max_automatic_bid", precision: 10, scale: 2
  end

  create_table "categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name",                    null: false
    t.integer  "category_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "views",       default: 0
  end

  create_table "evaluations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.decimal  "rate",                precision: 2, scale: 1, null: false
    t.string   "content",                                     null: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "evaluationable_type",                         null: false
    t.integer  "evaluationable_id",                           null: false
    t.integer  "user_id",                                     null: false
    t.string   "author_name"
    t.string   "author_nickname"
  end

  create_table "messages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "sender_id"
    t.string   "sender_name"
    t.string   "sender_nickname"
    t.integer  "receiver_id"
    t.string   "receiver_name"
    t.string   "receiver_nickname"
    t.string   "content"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "subject"
    t.integer  "user_id"
  end

  create_table "products", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.decimal  "immediate_price",    precision: 10, scale: 2
    t.datetime "date_bidding_end"
    t.decimal  "minimum_price",      precision: 10, scale: 2
    t.decimal  "start_price",        precision: 6,  scale: 2
    t.string   "title",                                                                     null: false
    t.string   "description",                                                               null: false
    t.datetime "created_at",                                                                null: false
    t.datetime "updated_at",                                                                null: false
    t.integer  "user_id",                                                                   null: false
    t.string   "image"
    t.integer  "category_id"
    t.decimal  "minimum_bid",        precision: 5,  scale: 2
    t.string   "status",                                      default: "wait_confirmation"
    t.integer  "views",                                       default: 0
    t.datetime "date_bidding_start"
    t.string   "buyer_name"
    t.integer  "buyer_id"
    t.integer  "buying_price"
    t.string   "buyer_nickname"
    t.string   "image1"
    t.string   "image2"
    t.string   "image3"
    t.string   "image4"
  end

  create_table "profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "first_name",        default: "anonymous", null: false
    t.string   "last_name",         default: "anonymous", null: false
    t.string   "image"
    t.string   "nickname",          default: "anonymous", null: false
    t.string   "name_of_the_house", default: "anonymous"
    t.integer  "street_number",     default: 0,           null: false
    t.string   "street_name",       default: "anonymous", null: false
    t.string   "city",              default: "anonymous", null: false
    t.integer  "postcode",          default: 11111,       null: false
    t.string   "country",           default: "anonymous", null: false
    t.datetime "date_of_birth"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "uid"
    t.string   "provider"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                            null: false
    t.string   "password_digest",                  null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "admin",            default: false, null: false
    t.string   "remember_digest"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.integer  "views",            default: 0
    t.string   "provider"
    t.string   "uid"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.integer  "profile_id"
    t.boolean  "ban",              default: false
  end

end
