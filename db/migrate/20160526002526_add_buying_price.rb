class AddBuyingPrice < ActiveRecord::Migration
  def change
    add_column :products, :buying_price, :integer
  end
end
