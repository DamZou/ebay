class SetColumnNull < ActiveRecord::Migration
  def change
    change_column_null :evaluations, :evaluationable_id, true
    change_column_null :evaluations, :evaluationable_type, true
  end
end
