class EvaluationsAuthor < ActiveRecord::Migration
  def change
    remove_column :evaluations, :author
    add_column :evaluations, :author_name, :string
    add_column :evaluations, :author_nickname, :string
  end
end
