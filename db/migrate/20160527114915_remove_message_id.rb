class RemoveMessageId < ActiveRecord::Migration
  def change
    remove_column :messages, :message_id
  end
end
