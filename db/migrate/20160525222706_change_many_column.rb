class ChangeManyColumn < ActiveRecord::Migration
  def change
    change_column_null :users, :profile_id, false
    change_column_null :profiles, :first_name, false
    change_column_null :profiles, :last_name, false
    change_column_null :profiles, :nickname, false
    change_column_null :profiles, :street_name, false
    change_column_null :profiles, :street_number, false
    change_column_null :profiles, :city, false
    change_column_null :profiles, :postcode, false
    change_column_null :profiles, :country, false
    change_column_null :profiles, :marital_status, false
    change_column_default :profiles, :date_of_birth, nil
  end
end
