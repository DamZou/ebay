class AddViewsColumnUsersProductsCategories < ActiveRecord::Migration
  def change
    add_column :users, :views, :integer, default: 0
    add_column :products, :views, :integer, default: 0
    add_column :categories, :views, :integer, default: 0
  end
end
