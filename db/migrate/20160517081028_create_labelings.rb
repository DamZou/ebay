class CreateLabelings < ActiveRecord::Migration
  def change
    create_table :labelings do |t|
      t.integer :label_id
      t.string :labelable_type
      t.integer :labelable_id

      t.timestamps null: false
    end
  end
end
