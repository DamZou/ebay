class AddColumnAdressIdAnsNicknameToUserRemoveAdress < ActiveRecord::Migration
  def change
    add_column :users, :adress_id, :integer, null: false
    add_column :users, :nickname, :string, null: false
    remove_column :users, :adress
  end
end
