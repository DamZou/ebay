class ChangeNameColumnProduct < ActiveRecord::Migration
  def change
    rename_column :products, :immediatePrice, :immediate_price
    rename_column :products, :dateBiddingEnd, :date_bidding_end
    rename_column :products, :minimumPrice, :minimum_price
    rename_column :products, :startPrice, :start_price
  end
end
