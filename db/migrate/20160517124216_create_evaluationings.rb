class CreateEvaluationings < ActiveRecord::Migration
  def change
    create_table :evaluationings do |t|
      t.integer :evaluation_id
      t.string :evaluationable_type
      t.integer :evaluationable_id

      t.timestamps null: false
    end
  end
end
