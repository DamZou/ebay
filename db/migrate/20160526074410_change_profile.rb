class ChangeProfile < ActiveRecord::Migration
  def change
  	add_column :profiles, :uid, :string
  	add_column :profiles, :provider, :string
  	change_column_null :users, :profile_id, true
  end
end
