class RemoveManyColumnToUser < ActiveRecord::Migration
  def change
    remove_column :users, :first_name
    remove_column :users, :last_name
    remove_column :users, :image
    remove_column :users, :nickname
    remove_column :users, :adress_id
  end
end
