class AddColumnTableEvaluations < ActiveRecord::Migration
  def change
    create_table :evaluations do |t|
      t.integer :rate
      t.string :content
      t.string :author

      t.timestamps null:false
    end
    add_column :evaluations, :evaluationable_type, :string
    add_column :evaluations, :evaluationable_id, :integer
  end
end
