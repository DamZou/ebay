class DefaultValuePostcode < ActiveRecord::Migration
  def change
    change_column_default :profiles, :postcode, 11111
  end
end
