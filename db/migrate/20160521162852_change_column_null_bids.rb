class ChangeColumnNullBids < ActiveRecord::Migration
  def change
    change_column_default :bids, :bid, false
    change_column_default :bids, :user_id, false
    change_column_default :bids, :product_id, false

  end
end
