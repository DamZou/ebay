class RemoveMaritalStatus < ActiveRecord::Migration
  def change
    remove_column :profiles, :marital_status
  end
end
