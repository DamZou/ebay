class MessageMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.message_mailer.report.subject
  def report(message)
    @message = message
    mail to: "damzou@gmail.com", subject: 'This message has been reported'
  end

end
