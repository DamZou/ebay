class SessionsFbController < ApplicationController
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    if user.id == nil
    	Profile.last.destroy
    	flash[:notice] = 'Email already use or invalid facebook account.'
      redirect_to login_path
    elsif user.ban == true
      flash[:notice] = 'User is ban'
      render 'new'
    else  
    session[:user_id] = user.id
    redirect_to root_url
	  end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end
end