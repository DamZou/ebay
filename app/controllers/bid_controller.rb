class BidController < ApplicationController
  before_action :correct_user, only: [:create_minimum_bid, :create_custom_bid, :create_max_automatic_bid]
  before_action :valid_bid, only: [:create_custom_bid, :create_max_automatic_bid]
  after_action :check_max_automatic_bid, only: [:create_minimum_bid, :create_custom_bid, :create_max_automatic_bid]

  def direct_purchase
    @bid = Bid.new
    @product = Product.find(params[:product_id])
    @bid.bid = @product.immediate_price
    @bid.user_id = current_user.id
    @bid.product_id = @product.id
    if @bid.save
      @product = Product.find(@product)
      @product.update(:status => 'sold')
      ProductMailer.sold(@product).deliver_now
      ProductMailer.buy(@product).deliver_now
    end
    redirect_to product_path(@product)
  end

  def create_minimum_bid
    @bid = Bid.new
    @product = Product.find(params[:product_id])
    if @product.bids.length == 0
      @bid.bid = @product.start_price + @product.minimum_bid
    else
      @bid.bid = @product.bid_in_progress(@product) + @product.minimum_bid
    end
    @bid.user_id = current_user.id
    @bid.product_id = @product.id
    if @bid.save
      if @product.immediate_price != nil && @product.immediate_price <= @bid.bid
        @product = Product.find(@product)
        @product.update(:status => 'sold')
        ProductMailer.sold(@product).deliver_now
        ProductMailer.buy(@product).deliver_now
      end  
      redirect_to product_path(@product) 
    else
      flash[:notice] = 'No minimum bid.'
      redirect_to products_path
    end  
  end

  def create_custom_bid
    @bid = Bid.new
    @product = Product.find(params[:product_id])
    if (@product.bid_in_progress(@product).to_i + @product.minimum_bid) > params[:bid].to_i
      flash[:notice] = 'Not enough bid.'
      redirect_to products_path
    else
      @bid.bid = params[:bid]
      @bid.user_id = current_user.id
      @bid.product_id = @product.id
    end
    if @bid.save
      if @product.immediate_price != nil && @product.immediate_price <= @bid.bid
        @product = Product.find(@product)
        @product.update(:status => 'sold')
        ProductMailer.sold(@product).deliver_now
        ProductMailer.buy(@product).deliver_now
      end
      redirect_to product_path(@product.id)
    end
  end

  def create_max_automatic_bid
    @bid = Bid.new
    @product = Product.find(params[:product_id])
    if @product.bid_in_progress(@product).to_i > params[:bid].to_i + @product.minimum_bid
      flash[:notice] = 'Not enough bid.'
      redirect_to products_path
    else
      @bid.max_automatic_bid = params[:bid]
      @bid.bid = @product.bid_in_progress(@product).to_i + @product.minimum_bid
      @bid.user_id = current_user.id
      @bid.product_id = @product.id
      if @bid.save
        if @product.immediate_price != nil && @product.immediate_price <= @bid.bid
          @product = Product.find(@product)
          @product.update(:status => 'sold')
          ProductMailer.sold(@product).deliver_now
          ProductMailer.buy(@product).deliver_now
        end
        redirect_to product_path(@product.id)
      end
    end
  end

  private

  def correct_user
    unless logged_in?
      flash[:notice] = 'Please log in.'
      redirect_to login_path
    end
    unless current_user.id != params[:product_id]
      flash[:notice] = 'This is your @product.'
      redirect_to products_path
    end
  end

  def check_max_automatic_bid
    bids = []
    bids_value = []
    product = Product.find(@product)
    winner_id = product.bid_winner(product).user_id
    if product.status == 'In selling'
      product.bids.each do |bid|
        if bid.max_automatic_bid != 0 && bid.max_automatic_bid.to_i > (product.bid_in_progress(product) + product.minimum_bid)
          bids.push(bid)
          bids_value.push(bid.max_automatic_bid)
        end
      end
      if bids.length == 1 && winner_id != bids[0].user_id
        bids[0].update(:bid => product.bid_in_progress(product) + product.minimum_bid)
      elsif bids.length > 1
        bids.each do |bid|
          if bid.max_automatic_bid == bids_value.min
            bid.update(:bid => bids_value.min)
            if product.immediate_price != nil && product.immediate_price <= bid.bid
              product.update(:status => 'sold')
              ProductMailer.sold(product).deliver_now
              ProductMailer.buy(product).deliver_now
            end
          elsif bid.max_automatic_bid > bids_value.min
            bid.update(:bid => bids_value.min + product.minimum_bid)
            if product.immediate_price != nil && product.immediate_price <= bid.bid
              @product = Product.find(@product)
              product.update(:status => 'sold')
              ProductMailer.sold(product).deliver_now
              ProductMailer.buy(product).deliver_now
            end
          end
        end
      end
    end
  end

  def valid_bid
    unless /\A\d*\z/.match(params[:bid])
      flash[:notice] = 'Invalid bid.'
      redirect_to products_path
    end
  end

end
