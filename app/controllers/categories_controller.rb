class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: :create
  before_action :logged_in_user, :admin_user, only: [:edit, :update, :destroy]

  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.order(:name).where("name like ?", "%#{params[:term]}%")
    respond_to do |format|
      format.html
      format.json { render json: @categories.map{ |tag| {:label => tag.name} }}
    end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    @products = @category.get_products(@category)
    views = @category.views+1
    @category.update_attributes(:views => views)
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  # GET /categories/1/new
  def new_child
    @category = Category.new
  end

  # GET /categories/1/edit
  def edit
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /categories/child
  # POST /categories/child.json
  def create_child
    @category = Category.new(category_params)
    @category.category_id = params[:category_id]
    parent_category = Category.find(params[:category_id])
    @category.name = parent_category.name + '_' + params[:category][:name]
    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new_child }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    def logged_in_user
      unless logged_in?
        flash[:notice] = 'Please log in.'
        redirect_to login_path
      end
    end

    def admin_user
      redirect_to(categories_path) && flash[:notice] = 'You are not an admin.' unless current_user.admin?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name, :category_id)
    end
end
