class ProfilesController < ApplicationController
  before_action :set_profile, :correct_user, only: [:edit, :update]

  # GET /profiles/new
  def new
    @profile = Profile.new
    @profile.build_user
  end

  # GET /profiles/1/edit
  def edit
    @user = User.find(@profile)
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to user_path(@profile), notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_profile
    @profile = Profile.find(params[:id])
  end

  def logged_in_user
    unless logged_in?
      flash[:notice] = 'Please log in.'
      redirect_to login_path
    end
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(users_path) && flash[:notice] = 'It is not you.' unless current_user?(@user)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def profile_params
    params.require(:profile).permit(:first_name, :last_name, :image, :nickname, :name_of_the_house, :street_number,
                                    :street_name, :city, :postcode, :country, :date_of_birth, :marital_status,
                                    :user_id, user_attributes:[:email, :password, :password_confirmation, :admin])
  end
end
