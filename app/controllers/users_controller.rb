class UsersController < ApplicationController
  before_action :set_user, only: [:show, :destroy, :ban, :deban]
  before_action :logged_in_user, only: :destroy
  before_action :correct_user, only: :destroy


  # GET /users
  # GET /users.json
  def index
    #@users = User.order(:nickname).where("nickname like ?", "%#{params[:term]}%")
    #respond_to do |format|
    #format.html
    #format.json { render json: @users.map{ |tag| {:label => tag.nickname} }}
    #end
    @users = User.all
  end

  def show
    views = @user.views+1
    @user.update_attributes(:views => views)
    @products = Product.where(:status => 'archive', :user_id => @user.id)
  end

  # GET /users/new
  def new
    if logged_in?
      flash[:notice] = 'You are already a member.'
      redirect_to users_path
    end
    @user = User.new
  end

  def ban
    @user.update(:ban => true)
    UserMailer.ban(@user).deliver_now
    flash[:notice] = 'Email was send to ' + @user.profile.first_name + ' ' + @user.profile.last_name + '.'
    redirect_to users_path
  end

  def deban
    @user.update(:ban => false)
    UserMailer.deban(@user).deliver_now
    flash[:notice] = 'Email was send to ' + @user.profile.first_name + ' ' + @user.profile.last_name + '.'
    redirect_to users_path
  end


  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @profile = Profile.new
    @profile.save
    @user.profile_id = @profile.id
    respond_to do |format|
      if @user.save
        UserMailer.register(@user).deliver_now
        log_in @user
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  def logged_in_user
    unless logged_in?
      flash[:notice] = 'Please log in.'
      redirect_to login_path
    end
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(users_path) && flash[:notice] = 'It is not you.' unless current_user?(@user)
  end

  def admin_user
    redirect_to(users_path) && flash[:notice] = 'You are not an admin.' unless current_user.admin?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:email, :ban,:password, :profile_id, :password_confirmation, :admin, :remember_token)
  end
end
