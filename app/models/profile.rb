class Profile < ActiveRecord::Base
  belongs_to :user

  mount_uploader :image, ImageUploader

  validates :postcode, format: {:with => /\A\d{5}\z/, :message => 'only five numbers' }
  validates :street_number, presence: true
  validates :street_name, presence: true
  validates :city, presence: true
  validates :nickname, presence: true
  validates :country, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true

end
