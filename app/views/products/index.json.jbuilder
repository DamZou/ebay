json.array!(@products) do |product|
  json.extract! product, :id, :immediatePrice, :dateBiddingEnd, :minimumPrice, :startPrice, :title, :description
  json.url product_url(product, format: :json)
end
