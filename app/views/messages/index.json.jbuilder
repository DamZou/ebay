json.array!(@messages) do |message|
  json.extract! message, :id, :message_id, :sender_id, :sender_name, :sender_nickname, :receiver_id, :receiver_name, :receiver_nickname, :content
  json.url message_url(message, format: :json)
end
