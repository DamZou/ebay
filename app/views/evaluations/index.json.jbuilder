json.array!(@evaluations) do |evaluation|
  json.extract! evaluation, :id, :rate, :content, :author
  json.url evaluation_url(evaluation, format: :json)
end
