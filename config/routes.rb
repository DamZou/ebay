Rails.application.routes.draw do

 
  get 'webmaster/webmaster', to: 'webmaster#webmaster', as: :webmaster

  # messages rounting
  get 'messages', to: 'messages#index', as: :messages
  get 'messages/:id/new' , to: 'messages#new', as: :message_new
  get 'messages/:id/respond' , to: 'messages#respond', as: :message_respond
  post 'messages/:id/create', to: 'messages#create', as: :message_create
  get 'messages/:id', to: 'messages#show', as: :message
  delete 'messages/:id/', to: 'messages#destroy', as: :message_destroy
  get 'messages/:id/report', to: 'messages#report', as: :message_report

  # profile routing
  resources :profiles, only: [:edit, :update]

  # search routing
  post 'search/common_search', to: 'search#common_search', as: :common_search
  get 'search', to: 'search#search', as: :search
  get 'search/google', to: 'search#google', as: :google

  # bidding routing
  post 'bid/:product_id/mini_bid', to: 'bid#create_minimum_bid', as: :new_minimum_bid
  post 'bid/:product_id/custom_bid', to: 'bid#create_custom_bid', as: :new_custom_bid
  post 'bid/:product_id/max_automatic_bid', to: 'bid#create_max_automatic_bid', as: :new_max_automatic_bid
  post 'bid/:product_id/direct_purchase', to: 'bid#direct_purchase', as: :purchase

  # categories routing
  resources :categories
  post 'categories/:category_id/new', to: 'categories#new_child', as: :new_child_category
  post 'categories/:category_id/create', to: 'categories#create_child', as: :create_child_category

  #  password reset routing
  resources :password_resets, only: [:new, :create, :edit, :update]

  # session routing
  get 'login' => 'session#new'
  post 'login' => 'session#create'
  delete 'logout' => 'session#destroy'

  # home
  get '' => 'products#index', as: :root

  # evaluations routing
  get 'evaluations', to: 'evaluations#index'
  post 'evaluations/:evaluationable_type/:evaluationable_id/create', to: 'evaluations#create', as: :create_eval
  get 'evaluations/:evaluationable_type/:evaluationable_id/new', to: 'evaluations#new', as: :new_eval
  get 'evaluations/:id/edit', to: 'evaluations#edit', as: :edit_evaluation
  get 'evaluations/:id', to: 'evaluations#show', as: :evaluation
  patch 'evaluations/:id', to: 'evaluations#update'
  put 'evaluations/:id', to: 'evaluations#update'
  delete 'evaluations/:id', to: 'evaluations#destroy'

  # products routing
  resources :products do
    get :autocomplete_category_name, :on => :collection
  end
  resources :products
  post 'products/:id/confirm', to: 'products#confirm', as: :confirm
  post 'products/:id/not_confirm', to: 'products#not_confirm', as: :not_confirm
  post 'products/:id/:bid_id/pay', to: 'products#pay', as: :pay
  get 'products/:id/more_days', to: 'products#seven_more_days', as: :more_days
  get 'archive', to: 'products#archive', as: :archive
  get 'recommend', to: 'products#recommend', as: :recommend

  # users routing
  resources :users, only: [:index, :new, :create, :show, :destroy]
  post 'users/:id/ban', to: 'users#ban', as: :user_ban
  post 'users/:id/deban', to: 'users#deban', as: :user_deban

  match 'auth/:provider/callback', to: 'sessions_fb#create', via: [:get, :post]
  match 'auth/failure', to: redirect('/'), via: [:get, :post]
  match 'signout', to: 'sessions_fb#destroy', as: 'signout', via: [:get, :post]

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     #   end
end
